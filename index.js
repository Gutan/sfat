/** @format */

import {React} from 'react';

import {AppRegistry, Platform} from 'react-native';
import {name as appName} from './app.json';
import App from './App';

AppRegistry.registerComponent(appName, () => App);
