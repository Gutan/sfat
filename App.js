/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from "react";
import {
  Platform,
  StyleSheet,
  Image,
  Text,
  View,
  Animated,
  Easing,
  Alert,
  Button
} from "react-native";
import style from "./src/styles/style.js";
import appColors from "./src/styles/colors.js";
import Intro from "./src/screens/Intro.js";
import IconButton from "./src/components/buttons/IconButton.js";
import AddButton from "./src/components/buttons/AddButton.js";
import Feed from "./src/screens/Feed.js";
import Profile from "./src/screens/Profile.js";
import Menu from "./src/screens/Menu.js";
import Answer from "./src/screens/Answer.js";
import CardStackStyleInterpolator from "react-navigation";

import {
  createBottomTabNavigator,
  createAppContainer,
  createStackNavigator
} from "react-navigation";

const tabNavigator = createBottomTabNavigator(
  {
    Feed: Feed,
    Plus: {
      screen: () => null,
      navigationOptions: ({ navigation }) => ({
        mode: "modal",
        headerMode: "none",
        tabBarOnPress: () => null,
        title: "",
        tabBarIcon: <AddButton navigation={navigation} /> // Plus button component
      })
    },
    Answer: Answer
  },
  {
    tabBarOptions: {
      labelStyle: {
        fontSize: 16,
        marginBottom: 12
      },
      activeTintColor: appColors.activeColor,
      inactiveTintColor: appColors.greyTextColor,
      style: {
        backgroundColor: appColors.bottomTabBackgroundColor
      }
    },
    navigationOptions: {
      tabBarOnPress: ({ previousScene, scene, jumpToIndex }) => {
        const { route, index, focused } = scene;
        jumpToIndex(0);
      }
    }
  }
);

const MainNavigator = createStackNavigator(
  {
    Intro: {
      screen: Intro,
      navigationOptions: {
        headerMode: "none",
        header: null
      }
    },
    Profile: {
      screen: Profile,
      navigationOptions: {
        headerTitle: () => <Text style={styles.headerText}>Profile</Text>,
        headerStyle: {
          backgroundColor: appColors.backgroundDark
        },
        headerTransparent: true,
        headerBackTitle: null,
        headerBackImage: () => (
          <IconButton buttonType="close" buttonHeight={30} buttonWidth={30} />
        )
      }
    },
    Menu: {
      screen: Menu,
      navigationOptions: {
        headerTitle: () => <Text style={styles.headerText}>Menu</Text>,
        headerStyle: {
          backgroundColor: appColors.backgroundDark
        },
        headerTransparent: true,
        headerBackTitle: null,
        headerBackImage: () => (
          <IconButton buttonType="close" buttonHeight={30} buttonWidth={30} />
        )
      }
    },
    QANavigator: {
      screen: tabNavigator,
      navigationOptions: ({ navigation }) => ({
        headerStyle: {
          backgroundColor: appColors.backgroundDark,
          borderBottomWidth: 0
        },
        headerBackTitle: null,
        title:
          typeof navigation.state.params === "undefined" ||
          typeof navigation.state.params.title === "undefined"
            ? ""
            : navigation.state.params.title,
        headerLeft: (
          <IconButton
            buttonType="menu"
            buttonHeight={30}
            buttonWidth={30}
            onClick={() => navigation.navigate("Menu")}
          />
        ),
        headerRight: (
          <IconButton
            buttonType="profile"
            buttonHeight={30}
            buttonWidth={30}
            onClick={() => navigation.navigate("Profile")}
          />
        )
      })
    }
  },
  {
    transitionConfig: () => ({
      transitionSpec: {
        duration: 200,
        easing: Easing.out(Easing.poly(4)),
        timing: Animated.timing
      },
      screenInterpolator: sceneProps => {
        const { layout, position, scene } = sceneProps;
        const { index } = scene;

        const height = layout.initHeight;
        const translateY = position.interpolate({
          inputRange: [index - 1, index, index + 1],
          outputRange: [height, 0, 0]
        });

        const opacity = position.interpolate({
          inputRange: [index - 1, index - 0.99, index],
          outputRange: [0, 1, 1]
        });

        return { opacity, transform: [{ translateY }] };
      }
    })
  }
);

const styles = StyleSheet.create({
  headerText: { color: appColors.almostWhite, fontSize: 20 }
});

const App = createAppContainer(MainNavigator);

export default App;
