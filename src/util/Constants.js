/*jshint esversion: 6 */
export const USER_ID_KEY = "userId";
export const USER_NAME_KEY = "userName";
export const VIEW_CORNER = 8;
export const BUTTON_CORNER = 24;
export const SHADOW_OPACITY = 0.5;
export const SHADOW_RADIUS = 1;
export const HOT = "hot";
export const TRENDING = "trending";
