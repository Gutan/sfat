const appColors = {
  primaryLight: "#883460",
  primaryButton: "#EF820D",
  purpleButton: "#841E65",
  redButton: "#CC3737",
  secondaryButton: "rgba(255,255,255,0.1)",
  secondaryButtonGray: "#D8D8D8",
  activeColor: "#EF820D",
  backgroundDark: "#3D1542",
  whiteBackground: "#E7E7E7",
  dialogHeader: "#4E223F",
  bottomTabBackgroundColor: "#EDEDED",
  dividerColor: "#9E9E9E",
  inputBackground: "#FBFBFB",
  almostWhite: "#F6F6F6",
  facebookButton: "#377ECC",
  toastColor: "#3D1542",
  failedColor: "#F11D1D",
  whiteTransparent: "rgba(255,255,255,0.9)",

  textColorGreyLight: "#9E9E9E",
  textColorGrey: this.backgroundDark,
  questionTextColor: "#525252",
  answerTextColor: "#6E5E69"
};

export default appColors;
