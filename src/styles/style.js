import {colors, fonts, padding, dimensions} from './base.js'
import {Platform} from 'react-native';

import {StyleSheet} from 'react-native'
import appColors from './colors.js';

export default StyleSheet.create({
  header: {
    fontSize: fonts.lg,
    fontFamily: fonts.primary
  },
  section: {
    paddingVertical: padding.lg,
    paddingHorizontal: padding.xl
  },
  topText: {
    // fontFamily: 'celevenia',
    textAlign: 'center',
    marginTop: 15,
    position: 'relative',
    fontSize: 16,
    color: appColors.textColorGreyLight,
    backgroundColor: 'transparent'
  },
  primaryButton: {
    padding: 10,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius:4,
    alignItems: 'center',
    backgroundColor: appColors.primaryButton,
    margin: 20,
    position: 'absolute',
    width: '92%',
    height: 50,
    bottom: 0,
    elevation: 2,
  },
  buttonText: {
    color: appColors.almostWhite,
    textAlign: 'center',
    fontSize: 20,
  },
  mainContainer :{
   flex:1,
   justifyContent: 'center',
   alignItems: 'center',
   marginTop: (Platform.OS == 'ios') ? 20 : 0
  },
})
