/*jshint esversion: 6 */
import React from "react";
import { Alert, Platform, StyleSheet, Dimensions, Text } from "react-native";
import { View } from "react-native-animatable";
import {
  createBottomTabNavigator,
  createStackNavigator
} from "react-navigation";
import * as Animatable from "react-native-animatable";
import * as Constants from "../util/Constants.js";
import appColors from "../styles/colors.js";

const TOAST_TIME_MILLIS = 2000;

var { height, width } = Dimensions.get("window");

export default class Toast extends React.Component {
  renderToast() {
    if (this.props.toastMessage) {
      setTimeout(this.props.onToastDone, TOAST_TIME_MILLIS);
      return (
        <View
          animation="fadeIn"
          duration={300}
          style={{
            position: "absolute",
            height: 50,
            left: 0,
            backgroundColor: appColors.toastColor,
            borderRadius: 24,
            right: 0,
            alignItems: "center",
            bottom: height / 2,
            justifyContent: "center",
            shadowColor: "black",
            shadowOffset: { width: 0, height: 1 },
            shadowOpacity: Constants.SHADOW_OPACITY,
            shadowRadius: Constants.SHADOW_RADIUS
          }}
        >
          <Text
            style={{
              color: appColors.almostWhite,
              marginHorizontal: 10,
              justifyContent: "center",
              textAlign: "center"
            }}
          >
            {this.props.toastMessage}
          </Text>
        </View>
      );
    } else {
      return null;
    }
  }
  render() {
    return this.renderToast();
  }
}
