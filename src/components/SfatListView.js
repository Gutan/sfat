/*jshint esversion: 6 */
import React from "react";
import { View, FlatList, StyleSheet, Text, RefreshControl } from "react-native";
import ListViewItem from "./ListViewItem";

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

export default class SfatListView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false
    };
  }

  onRefresh() {
    this.setState({ refreshing: true });
    fetchData().then(() => {
      this.setState({ refreshing: false });
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this.state.onPullToRefresh}
            />
          }
          data={this.props.feedList}
          renderItem={({ item }) => (
            <ListViewItem
              onNoUserId={this.props.onNoUserId}
              navigation={this.props.navigation}
              likeId={item.likeId}
              likes={item.likes}
              dislikes={item.dislikes}
              qUser={item.qUser}
              aUser={item.aUser}
              question={item.question}
              answer={item.answer}
              ourLike={item.ourLike}
              answerId={item.answerId}
            />
          )}
        />
      </View>
    );
  }
}
