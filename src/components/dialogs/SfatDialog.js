/*jshint esversion: 6 */
import React from "react";
import {
  Alert,
  Platform,
  StyleSheet,
  Modal,
  TextInput,
  Dimensions,
  Image,
  Text,
  View,
  TouchableOpacity
} from "react-native";
import * as Constants from "../../util/Constants.js";
import appColors from "../../styles/colors.js";

export default class SfatDialog extends React.Component {
  render() {
    return (
      <Modal
        visible={this.props.alertVisibility}
        transparent={true}
        animationType={"fade"}
      >
        <View
          style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
            backgroundColor: "rgba(0,0,0,0.6)"
          }}
        >
          <View style={modalStyles.mainView}>
            <View style={modalStyles.header}>
              <Text style={modalStyles.titleText}>
                {this.props.dialogTitle}
              </Text>
            </View>
            <Text style={modalStyles.dialogText}>{this.props.dialogText}</Text>
            <View style={modalStyles.buttonsLayout}>
              <TouchableOpacity
                style={modalStyles.negButtonStyle}
                onPress={this.props.onNegButtonClick}
                activeOpacity={0.7}
              >
                <Text style={modalStyles.negButtonText}>
                  {this.props.negText}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={modalStyles.pozButtonStyle}
                onPress={this.props.onPozButtonClick}
                activeOpacity={0.7}
              >
                <Text style={modalStyles.pozButtonText}>
                  {this.props.pozText}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}

const modalStyles = StyleSheet.create({
  header: {
    backgroundColor: appColors.dialogHeader,
    alignItems: "center",
    borderTopLeftRadius: Constants.VIEW_CORNER,
    borderTopRightRadius: Constants.VIEW_CORNER,
    shadowColor: "black",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: Constants.SHADOW_OPACITY,
    shadowRadius: Constants.SHADOW_RADIUS
  },
  mainView: {
    flexDirection: "column",
    backgroundColor: appColors.whiteBackground,
    borderRadius: Constants.VIEW_CORNER,
    width: "90%"
  },
  titleText: {
    margin: 16,
    fontFamily: "Arial Rounded MT Bold",
    fontSize: 14,
    color: "#fff"
  },
  dialogText: {
    marginVertical: 32,
    marginHorizontal: 16,
    textAlign: "center",
    fontFamily: "Arial Rounded MT Bold",
    fontSize: 14,
    color: appColors.textColorGrey
  },
  buttonsLayout: {
    alignItems: "center",
    flexDirection: "row",
    marginBottom: 16,
    justifyContent: "space-around",
    width: "100%"
  },
  pozButtonStyle: {
    backgroundColor: appColors.primaryButton,
    width: 140,
    height: 35,
    borderRadius: 4,
    justifyContent: "center",
    alignItems: "center",
    shadowColor: "black",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: Constants.SHADOW_OPACITY,
    shadowRadius: Constants.SHADOW_RADIUS
  },
  negButtonStyle: {
    backgroundColor: appColors.secondaryButtonGray,
    width: 140,
    height: 35,
    borderRadius: 4,
    justifyContent: "center",
    alignItems: "center",
    shadowColor: "black",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: Constants.SHADOW_OPACITY,
    shadowRadius: Constants.SHADOW_RADIUS
  },
  negButtonText: {
    color: appColors.textColorGrey,
    textAlign: "center",
    fontSize: 16
  },
  pozButtonText: {
    color: appColors.almostWhite,
    textAlign: "center",
    fontSize: 16
  },
  baseInput: {
    height: 100,
    margin: 16,
    backgroundColor: appColors.inputBackground,
    borderRadius: Constants.VIEW_CORNER,
    shadowColor: "black",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: Constants.SHADOW_OPACITY,
    shadowRadius: Constants.SHADOW_RADIUS
  }
});
