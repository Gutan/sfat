/*jshint esversion: 6 */
import React from "react";
import {
  Alert,
  Platform,
  StyleSheet,
  Image,
  Text,
  View,
  TouchableOpacity
} from "react-native";
import * as Constants from "../../util/Constants.js";
import appColors from "../../styles/colors.js";

export default class LongButton extends React.Component {
  render() {
    return (
      <TouchableOpacity
        style={[
          {
            backgroundColor: appColors.primaryButton,
            width: "90%",
            height: 45,
            marginTop: 16,
            borderRadius: Constants.BUTTON_CORNER,
            justifyContent: "center",
            alignItems: "center",
            shadowColor: "black",
            shadowOffset: { width: 0, height: 1 },
            shadowOpacity: Constants.SHADOW_OPACITY,
            shadowRadius: Constants.SHADOW_RADIUS
          },
          this.props.buttonStyle
        ]}
        onPress={this.props.clickListener}
        activeOpacity={0.7}
      >
        <Text
          style={[
            { color: appColors.almostWhite, textAlign: "center", fontSize: 16 },
            this.props.textStyle
          ]}
        >
          {this.props.buttonText}
        </Text>
      </TouchableOpacity>
    );
  }
}
