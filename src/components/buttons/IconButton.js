/*jshint esversion: 6 */
import React from "react";
import {
  Alert,
  Platform,
  StyleSheet,
  Image,
  Text,
  View,
  TouchableOpacity
} from "react-native";

export default class IconButton extends React.Component {
  render() {
    imageUri = getUriByType(this.props.buttonType);
    thisObj = this;
    return (
      <TouchableOpacity
        style={[
          {
            width: this.props.buttonWidth,
            height: this.props.buttonHeight,
            margin: 20
          },
          this.props.buttonStyle
        ]}
        onPress={this.props.onClick}
      >
        <Image
          style={{
            flex: 1,
            width: null,
            height: null,
            resizeMode: "contain"
          }}
          source={imageUri}
        />
      </TouchableOpacity>
    );
  }
}

let getUriByType = function(buttonType) {
  switch (buttonType) {
    case "menu":
      return require(`../../images/icon_menu.png`);
    case "profile":
      return require(`../../images/icon_profile.png`);
    case "plus":
      return require(`../../images/add_button.png`);
    case "close":
      return require(`../../images/icon_close.png`);
    case "back":
      return require(`../../images/icon_chevron.png`);
    default:
      return null;
  }
};
