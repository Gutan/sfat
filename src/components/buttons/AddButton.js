/*jshint esversion: 6 */
import React from "react";
import {
  Alert,
  Platform,
  StyleSheet,
  Modal,
  TextInput,
  Dimensions,
  Image,
  Text,
  TouchableOpacity,
  ActivityIndicator
} from "react-native";
import { View } from "react-native-animatable";
import {
  createBottomTabNavigator,
  createStackNavigator
} from "react-navigation";
import * as Animatable from "react-native-animatable";
import * as Constants from "../../util/Constants.js";
import appColors from "../../styles/colors.js";
import { pushQuestion } from "../../managers/FirebaseManager";
import QuestionDialog from "../dialogs/QuestionDialog.js";
import SfatDialog from "../dialogs/SfatDialog.js";
import Toast from "../Toast.js";
import { getUserId, navigateToLogin } from "../../AppMaster.js";

const TOAST_TIME_MILLIS = 2000;

var { height, width } = Dimensions.get("window");

export default class AddButton extends React.Component {
  constructor() {
    super();
    this.state = {
      alertVisibility: false,
      dialogVisibility: false,
      loadingVisibility: false,
      isSendingQuestion: false,
      toastMessage: ""
    };
  }

  /**
  The callback used by firebase when the db is done updating.
  @param error The error if the operation failed and null if succeeded
  */
  putQuestionCallback(error) {
    //callback called, means it was sent or error.
    this.setState({ isSendingQuestion: false, question: null });
    if (!error) {
      this.setState({
        alertVisibility: false,
        toastMessage: "Question sent!"
      });
    } else {
      this.setState({
        toastMessage: error
      });
    }
  }

  /**
  Click listener for Question dialog positive button
  */
  onPozButtonClick() {
    let userId = getUserId(this.props.navigation);
    this.setState({ alertVisibility: false, isSendingQuestion: true });
    if (userId) {
      if (this.state.question) {
        pushQuestion(
          this.state.question,
          new Date().toDateString(),
          userId,
          this.putQuestionCallback.bind(this)
        );
        setTimeout(() => {
          //If after 5 seconds, it's still sending, fuck it, mark as sent
          //todo rething the logic.
          if (this.state.isSendingQuestion) {
            this.putQuestionCallback(null);
          }
        }, 2000);
      } else {
        alert("Please write a question.");
      }
    } else {
      //not logged it, ask to go to login screen
      this.setState({
        dialogTitle: "Logged out",
        dialogText: "Continue to login screen?",
        pozText: "Yes",
        negText: "Cancel",
        onPozButtonClick: (() => {
          this.setState({ dialogVisibility: false });
          navigateToLogin(this.props.navigation);
        }).bind(this),
        onNegButtonClick: (() => {
          this.setState({ dialogVisibility: false });
        }).bind(this),
        dialogVisibility: true
      });
    }
  }

  /**
  Click listener for Question dialog negative button
  */
  onNegButtonClick() {
    this.setState({ alertVisibility: false });
  }

  /**
  The callback used by input view when the text has changed
  @param question The most recent question in edit field
  */
  onInputTextChanged(question) {
    this.setState({ question });
  }

  /**
  The callback used by Toast view when showing.
  */
  onToastDone() {
    this.setState({
      toastMessage: ""
    });
  }

  /**
  Renders the loading view
  */
  renderLoading() {
    if (this.state.loadingVisibility) {
      return (
        <View
          style={{
            position: "absolute",
            height: 50,
            left: 0,
            backgroundColor: appColors.whiteTransparent,
            borderRadius: 4,
            right: 0,
            alignItems: "center",
            bottom: height / 2,
            justifyContent: "center"
          }}
        >
          <ActivityIndicator size="large" color={appColors.activeColor} />
        </View>
      );
    } else {
      return null;
    }
  }

  render() {
    return (
      <View>
        <Toast
          toastMessage={this.state.toastMessage}
          onToastDone={this.onToastDone.bind(this)}
        />
        <QuestionDialog
          onPozButtonClick={this.onPozButtonClick.bind(this)}
          onNegButtonClick={this.onNegButtonClick.bind(this)}
          alertVisibility={this.state.alertVisibility}
          onInputTextChanged={this.onInputTextChanged.bind(this)}
        />
        <SfatDialog
          onPozButtonClick={this.state.onPozButtonClick}
          onNegButtonClick={this.state.onNegButtonClick}
          alertVisibility={this.state.dialogVisibility}
          dialogTitle={this.state.dialogTitle}
          dialogText={this.state.dialogText}
          pozText={this.state.pozText}
          negText={this.state.negText}
        />
        <TouchableOpacity
          style={{
            width: 70,
            height: 70,
            margin: 20
          }}
          onPress={() => this.setState({ alertVisibility: true })}
        >
          <Image
            style={{
              flex: 1,
              width: null,
              height: null,
              resizeMode: "contain"
            }}
            source={require(`../../images/add_button.png`)}
          />
        </TouchableOpacity>
      </View>
    );
  }
}
