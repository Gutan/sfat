/*jshint esversion: 6 */
import React from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity } from "react-native";
import appColors from "../styles/colors.js";
import * as Constants from "../util/Constants.js";
import { pushLike, removeLike } from "../managers/FirebaseManager";

const colors = ["#0079BE", "#D29034", "#B04632", "#89609E"];
const ICON_HEIGHT = 20;
const ICON_WIDTH = 18;

import { getUserId, navigateToLogin } from "../AppMaster.js";

lastColor = "";
let userId;

/**
Returns a random color fron the list of colors.
*/
getRandomColor = () => {
  let newColor = lastColor;
  while (lastColor == newColor) {
    newColor = colors[Math.floor(Math.random() * colors.length)];
  }
  lastColor = newColor;
  return newColor;
};

export default class ListViewItem extends React.Component {
  /* Return object for populate the list */
  constructor(props) {
    super(props);
    this.state = {
      likeId: this.props.likeId,
      likes: this.props.likes,
      dislikes: this.props.dislikes,
      qUser: this.props.qUser,
      aUser: this.props.aUser,
      question: this.props.question,
      answer: this.props.answer,
      ourLike: this.props.ourLike, //0 - none, 1 - like, 2 - dislike.
      answerId: this.props.answerId,
      color: getRandomColor(),
      likeIsUpdating: false
    };
  }

  //todo move this logic to onLikeClick and test.
  liked(up) {
    userId = getUserId();
    if (!this.state.likeIsUpdating) {
      if (userId) {
        this.onLikeClick(up);
        this.sendLikeToServer(userId, up);
      } else {
        this.props.onNoUserId();
      }
    }
  }

  onLikeClick(up) {
    //New value for our action on the post (like, dislike, none);
    var newOurLike;
    var newLikes = this.state.likes;
    var newDislikes = this.state.dislikes;
    if (up) {
      newOurLike = this.state.ourLike == 1 ? 0 : 1;
      newLikes = newOurLike == 1 ? this.state.likes + 1 : this.state.likes - 1;
      if (newOurLike == 1 && this.state.ourLike == 2) {
        newDislikes = this.state.dislikes - 1;
      }
    } else {
      newOurLike = this.state.ourLike == 2 ? 0 : 2;
      newDislikes =
        newOurLike == 2 ? this.state.dislikes + 1 : this.state.dislikes - 1;
      if (newOurLike == 2 && this.state.ourLike == 1) {
        newLikes = this.state.likes - 1;
      }
    }

    this.setState({
      likes: newLikes,
      dislikes: newDislikes,
      ourLike: newOurLike
    });
  }

  sendLikeToServer(userId, up) {
    //Remove if clicked again on an already liked or disliked button.
    if (userId) {
      this.setState({ likeIsUpdating: true });
      if ((this.state.ourLike == 1 && up) || (this.state.ourLike == 2 && !up)) {
        removeLike(this.state.likeId, error => {
          this.setState({ likeIsUpdating: false });
          if (error) {
            alert("Failed to remove like: " + error);
          } else {
            this.setState({ likeId: undefined });
          }
        });
      } else {
        pushLike(
          this.state.likeId,
          userId,
          up,
          this.state.answerId,
          (docId, error) => {
            this.setState({ likeIsUpdating: false });
            if (docId) {
              this.setState({
                likeId: docId
              });
            } else {
              this.onLikeClick(up);
              alert("Failed to push like: " + error);
            }
          }
        );
      }
    } else {
      console.log("Can't like, no usedId.");
    }
  }

  render() {
    return (
      <View style={styles.main}>
        <View style={styles.qaView}>
          <Text style={styles.question}>{this.state.question}</Text>
          <View style={styles.divider} />
          <Text style={styles.answer}>{this.state.answer}</Text>
        </View>
        <View style={[styles.bottomBar, { backgroundColor: this.state.color }]}>
          <View
            style={{
              flex: 1,
              alignItems: "center",
              marginLeft: 4,
              flexDirection: "row"
            }}
          >
            <Text style={styles.userName}>{this.state.aUser}</Text>
          </View>
          <View
            style={{
              marginVertical: 5,
              alignItems: "center",
              flexDirection: "row",
              marginRight: 5
            }}
          >
            <TouchableOpacity
              onPress={() => {
                this.liked(true);
              }}
            >
              <Image
                style={styles.thumbImage}
                source={
                  this.state.ourLike == 1
                    ? require(`../images/thumbs_up_active.png`)
                    : require(`../images/thumbs_up.png`)
                }
              />
            </TouchableOpacity>
            <Text style={styles.likes}>{this.state.likes}</Text>
            <TouchableOpacity
              onPress={() => {
                this.liked(false);
              }}
            >
              <Image
                style={styles.thumbImage}
                source={
                  this.state.ourLike == 2
                    ? require(`../images/thumbs_down_active.png`)
                    : require(`../images/thumbs_down.png`)
                }
              />
            </TouchableOpacity>
            <Text style={styles.likes}>{this.state.dislikes}</Text>
            <TouchableOpacity
              onPress={() => {
                alert("Shared");
              }}
            >
              <Image
                style={styles.thumbImage}
                source={require("../images/share.png")}
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: appColors.whiteBackground,
    flexDirection: "column",
    marginLeft: 8,
    marginRight: 8,
    marginTop: 8,
    marginBottom: 4,
    borderRadius: Constants.VIEW_CORNER,
    elevation: 2
  },
  userName: {
    fontSize: 12,
    color: appColors.almostWhite,
    margin: 5
  },
  likes: {
    fontSize: 10,
    color: appColors.almostWhite
  },
  question: {
    textAlign: "center",
    fontSize: 16,
    color: appColors.questionTextColor,
    marginTop: 8,
    marginHorizontal: 12
  },
  divider: {
    borderBottomColor: appColors.dividerColor,
    borderBottomWidth: 1,
    marginTop: 8,
    width: "80%"
  },
  answer: {
    textAlign: "center",
    fontSize: 14,
    marginHorizontal: 12,
    color: appColors.answerTextColor,
    marginVertical: 8
  },
  bottomBar: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    borderBottomLeftRadius: Constants.VIEW_CORNER,
    borderBottomRightRadius: Constants.VIEW_CORNER
  },
  qaView: {
    flex: 1.6,
    marginVertical: 8,
    flexDirection: "column",
    justifyContent: "space-around",
    alignItems: "center"
  },
  thumbImage: {
    width: ICON_WIDTH,
    height: ICON_HEIGHT,
    marginHorizontal: 6,
    resizeMode: "contain",
    shadowColor: "black",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: Constants.SHADOW_OPACITY,
    shadowRadius: Constants.SHADOW_RADIUS
  }
});
