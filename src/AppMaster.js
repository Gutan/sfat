/*jshint esversion:6 */
import DefaultPreference from "react-native-default-preference";
import * as Constants from "./util/Constants.js";
import { NavigationActions, StackActions } from "react-navigation";

export function initialize(onDone) {
  DefaultPreference.get(Constants.USER_ID_KEY)
    .then(
      function(value) {
        global.userId = value;
      }.bind(this)
    )
    .then(
      DefaultPreference.get(Constants.USER_NAME_KEY)
        .then(
          function(value) {
            global.userName = value;
          }.bind(this)
        )
        .then(
          function() {
            onDone(global.userId, global.userName);
          }.bind(this)
        )
    );
}
export function setUserId(userId, onDone) {
  global.userId = userId;
  DefaultPreference.set(Constants.USER_ID_KEY, userId).then(onDone);
}
export function setUserName(userName, onDone) {
  global.userName = userName;
  DefaultPreference.set(Constants.USER_NAME_KEY, userName).then(onDone);
}
export function getUserId() {
  return global.userId;
}
export function getUserName() {
  return global.userName;
}
export function navigateToLogin(navigation) {
  const resetAction = StackActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({ routeName: "Intro" })]
  });
  navigation.dispatch(resetAction);
}
