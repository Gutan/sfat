/*jshint esversion: 8 */
import firebase from "../config/Firebase";

import {
  LoginButton,
  LoginManager,
  GraphRequest,
  GraphRequestManager,
  AccessToken
} from "react-native-fbsdk";

const QUESTIONS = "questions";
const ANSWERS = "answers";
const LIKES = "likes";
const POBLIC_PROFILE = "public_profile";

const facebookLogin = async onFacebookLoginDone => {
  // Attempt a login using the Facebook login dialog asking for default permissions.
  LoginManager.logInWithReadPermissions([POBLIC_PROFILE]).then(
    function(result) {
      if (result.isCancelled) {
        console.log("Login cancelled");
      } else {
        console.log(
          "Login success with permissions: " +
            result.grantedPermissions.toString()
        );
        AccessToken.getCurrentAccessToken().then(data => {
          const infoRequest = new GraphRequest(
            "/me?fields=name,picture",
            null,
            (error, result) => {
              onFacebookLoginDone(result, error);
            }
          );
          // Start the graph request.
          new GraphRequestManager().addRequest(infoRequest).start();
        });
      }
    },
    function(error) {
      console.log("Login fail with error: " + error);
    }
  );
};

export { facebookLogin };
