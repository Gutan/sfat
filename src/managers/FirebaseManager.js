/*jshint esversion: 6 */
import firebase from "../config/Firebase";

const QUESTIONS = "questions";
const ANSWERS = "answers";
const LIKES = "likes";
const USERS = "users";

export function pushQuestion(text, tStamp, uId, doneCallback) {
  db = firebase.firestore();
  this.ref = db.collection(QUESTIONS);
  this.ref
    .add({
      qText: text,
      timeStamp: tStamp,
      userId: uId
    })
    .then(docRef => {
      // Alert.alert("ID", docRef.id);
      // console.error("docRef: ", docRef.id); //This crashes but shows the ID in error log
      doneCallback(null);
    })
    .catch(error => {
      console.error("Error adding document: ", error);
      doneCallback(error);
    });
}

export function pushAnswer(text, tStamp, uId, qId, doneCallback) {
  db = firebase.firestore();
  this.ref = db.collection(ANSWERS);
  this.ref
    .add({
      aText: text,
      timeStamp: tStamp,
      userId: uId,
      qId: qId
    })
    .then(docRef => {
      doneCallback(null);
    })
    .catch(error => {
      doneCallback(error);
    });
}

/**
 * push a like to server
 * @param {string} aId - answer id
 * @param {bool} likeBool - liked or disliked.
 * @param {string} userId - Our user id.
 * @param {function} doneCallback - the done callback.
 *
 * likes [
 *    aId : {
 *      userId : "Some id",
 *      like : true
 *    }, ...
 * ]
 */
export function pushLike(likeId, userId, likeBool, aId, doneCallback) {
  let newLike = {
    answerId: aId,
    userId: userId,
    like: likeBool
  };
  let promise;
  let collection = firebase.firestore().collection(LIKES);
  let docId;
  if (likeId) {
    let doc = collection.doc(likeId);
    docId = doc.id;
    promise = doc.update(newLike);
  } else {
    promise = collection.add(newLike);
  }
  promise
    .then(docRef => {
      if (docRef) {
        docId = docRef.id;
      }
      doneCallback(docId);
    })
    .catch(error => {
      doneCallback(null, error);
    });
}

export function pushUserName(docId, userName, userId, doneCallback) {
  if (!userId || !userName) {
    alert("pushUserName failed: " + userName);
    return;
  }
  let newUser = {
    userName: userName,
    userId: userId
  };
  let promise;
  let collection = firebase.firestore().collection(USERS);
  if (docId) {
    let doc = collection.doc(docId);
    promise = doc.update(newUser);
  } else {
    promise = collection.add(newUser);
  }
  promise
    .then(docRef => {
      console.log("docRef: " + docRef);
      if (docRef) {
        docId = docRef.id;
      }
      doneCallback(null);
    })
    .catch(error => {
      console.log("error: " + error);
      doneCallback(error);
    });
}

export function getUserById(userId, doneCallback) {
  firebase
    .firestore()
    .collection(USERS)
    .where("userId", "==", userId)
    .get()
    .then(function(querySnapshot) {
      let foundUserDoc;
      if (querySnapshot.length > 1) {
        alert("Got more than 1 user with userId:" + userId);
      } else {
        querySnapshot.forEach(function(doc) {
          foundUserDoc = doc;
        });
        doneCallback(foundUserDoc, null);
      }
    })
    .catch(function(error) {
      doneCallback(null, error);
    });
}
export function getUserByName(userName, doneCallback) {
  firebase
    .firestore()
    .collection(USERS)
    .where("userName", "==", userName)
    .get()
    .then(function(querySnapshot) {
      let foundUserDoc;
      if (querySnapshot.length > 1) {
        alert("Got more than 1 user with userName:" + userName);
      } else {
        querySnapshot.forEach(function(doc) {
          foundUserDoc = doc;
        });
        console.log("calling doneCallback:" + foundUserDoc.data());
        doneCallback(foundUserDoc, null);
      }
    })
    .catch(function(error) {
      console.log("calling doneCallback with error:" + error);
      doneCallback(null, error);
    });
}

export function removeLike(likeId, doneCallback) {
  let promise;
  let collection = firebase.firestore().collection(LIKES);
  if (likeId) {
    collection
      .doc(likeId)
      .delete()
      .then(docRef => {
        doneCallback(null);
      })
      .catch(error => {
        doneCallback(error);
      });
  } else {
    doneCallback("Need doc id to remove.");
  }
}

export function getQuestions(limit, doneCallback) {
  db = firebase.firestore();
  if (limit && doneCallback) {
    db.collection(QUESTIONS)
      .get()
      .then(snapshot => {
        let questionList = [];
        snapshot.forEach(doc => {
          questionList.push(doc);
        });
        doneCallback(questionList, null);
      })
      .catch(err => {
        console.log("Error getting documents", err);
      });
  } else {
    console.error("Error, invalid params: " + limit + "," + doneCallback);
  }
}

export function getAnswerPairs(userId, limit, doneCallback) {
  db = firebase.firestore();
  let promises = [];
  //Load all the likes as a map:  answerid = [Likes];
  let loadedLikes = {};
  //Load all the users as a map: userId:userName
  let loadedUsers = {};

  //add a promise to load users
  promises.push(
    db
      .collection(USERS)
      .get()
      .then(usersSnapshot => {
        usersSnapshot.forEach(userDoc => {
          let userId = userDoc.data().userId;
          let userName = userDoc.data().userName;
          loadedUsers[userId] = userName;
        });
      })
  );

  //add a promise to load likes
  promises.push(
    db
      .collection(LIKES)
      .get()
      .then(likesSnapshot => {
        likesSnapshot.forEach(likeDoc => {
          //Check if we already have a metadata for this like's answer id.
          ansMetadata = loadedLikes[likeDoc.data().answerId];
          //Create new metadata if not present for this answer id.
          if (!ansMetadata) {
            ansMetadata = {
              likeNumber: 0,
              dislikeNumber: 0,
              myLike: 0,
              myLikeId: 0
            };
          }
          var likeNumber = 0;
          var dislikeNumber = 0;
          //Count likes and dislikes for this answer metadata
          if (likeDoc.data().like) {
            ansMetadata.likeNumber++;
          } else {
            ansMetadata.dislikeNumber++;
          }
          //Is this like/dislike ours?
          isThisOurLike = likeDoc.data().userId === userId;

          if (isThisOurLike) {
            //0 - none, 1 - our like, 2 - our dislike.
            ansMetadata.myLike = likeDoc.data().like ? 1 : 2;
            //Our like doc id.
            ansMetadata.myLikeId = likeDoc.id;
          }
          ansMetadata.like = likeDoc.data().like;
          loadedLikes[likeDoc.data().answerId] = ansMetadata;
        });
      })
  );
  //Wait for likes and users data to load
  Promise.all(promises).then(data => {
    let loadedAnswers = {};
    let answerPairs = [];
    let promises = [];
    //Get all the answers
    db.collection(ANSWERS)
      .get()
      .then(answersSnapshot => {
        answersSnapshot.forEach(ansDoc => {
          loadedAnswers[ansDoc.id] = ansDoc.data();
          // console.log("Found answer: " + ansDoc.data().atext);
          keyCount = 0;
          promises.push(
            db
              .collection(QUESTIONS)
              .doc(ansDoc.data().qId)
              .get()
              .then(quesDoc => {
                ans = loadedAnswers[ansDoc.id];
                question = quesDoc.data();
                ansMetadata = loadedLikes[ansDoc.id];
                likeNumber = 0;
                dislikeNumber = 0;
                myLike = 0;
                myLikeId = 0;
                if (ansMetadata) {
                  likeNumber = ansMetadata.likeNumber;
                  dislikeNumber = ansMetadata.dislikeNumber;
                  myLike = ansMetadata.myLike;
                  myLikeId = ansMetadata.myLikeId;
                }
                let qUserName = loadedUsers[question.userId];
                let aUserName = loadedUsers[ans.userId];
                if (!qUserName) {
                  console.log(
                    "Can't find user name for userId: " + question.userId
                  );
                }
                if (!aUserName) {
                  console.log("Can't find user name for userId: " + ans.userId);
                }

                let item = {
                  key: (keyCount++).toString(),
                  likeId: myLikeId,
                  likes: likeNumber,
                  dislikes: dislikeNumber,
                  qUser: qUserName,
                  aUser: aUserName,
                  question: question.qText,
                  answer: ans.aText,
                  ourLike: myLike,
                  answerId: ansDoc.id
                };

                answerPairs.push(item);
              })
          );
        });
        Promise.all(promises).then(data => {
          doneCallback(answerPairs, null);
        });
      })
      .catch(err => {
        console.log("Error getting documents", err);
      });
  });
}
