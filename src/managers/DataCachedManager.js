/*jshint esversion: 6 */

import { getAnswerPairs } from "./FirebaseManager.js";

let cachedHotFeed;
let cachedTrendingFeed;
let onDoneCallBackHot;
let onDoneCallBackTrending;

export function cacheFeedData(userId) {
  cacheUpdatedHotFeed(userId);
  cacheUpdatedTrendingFeed(userId);
}

function cacheUpdatedHotFeed(userId) {
  getAnswerPairs(userId, 0, (data, error) => {
    cachedHotFeed = data;
    if (onDoneCallBackHot) {
      onDoneCallBackHot(cachedHotFeed);
      onDoneCallBackHot = undefined;
    }
  });
}
function cacheUpdatedTrendingFeed(userId) {
  getAnswerPairs(userId, 0, (data, error) => {
    cachedTrendingFeed = data;
    if (onDoneCallBackTrending) {
      onDoneCallBackTrending(cachedTrendingFeed);
      onDoneCallBackTrending = undefined;
    }
  });
}

export function getHotFeed(callback) {
  if (cachedHotFeed) {
    callback(cachedHotFeed);
  } else {
    onDoneCallBackHot = callback;
  }
}

export function getTrendingFeed(callback) {
  if (cachedTrendingFeed) {
    callback(cachedTrendingFeed);
  } else {
    onDoneCallBackTrending = callback;
  }
}
