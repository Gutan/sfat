/*jshint esversion: 6 */
import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Image,
  Text,
  View,
  TextInput,
  Button,
  Alert,
  Dimensions,
  InteractionManager,
  TouchableOpacity
} from "react-native";
import * as Animatable from "react-native-animatable";
import appColors from "../styles/colors.js";
import SfatListView from "../components/SfatListView.js";
import { getHotFeed, getTrendingFeed } from "../managers/DataCachedManager.js";
import * as Constants from "../util/Constants.js";

export default class TabScreen extends React.Component {
  /* Return object for populate the list */
  constructor(props) {
    super(props);

    this.state = {
      hotFeedItems: [],
      trendingFeedItems: []
    };
  }

  onPullToRefresh() {
    //todo make this method refresh the cached feed but don't get, use observer pattern.
    console.log("onPullToRefresh");
    getHotFeed(data => {
      console.log("onPullToRefresh, new hot data: " + data.length);
      this.setState({
        hotFeedItems: data
      });
    });
    getTrendingFeed(data => {
      this.setState({
        trendingFeedItems: data
      });
    });
  }

  componentDidMount() {
    const { screenName } = this.props;
    switch (screenName) {
      case Constants.HOT:
        //todo use observer pattern.
        getHotFeed(data => {
          this.setState({
            hotFeedItems: data
          });
        });
        break;
      case Constants.TRENDING:
        //todo use observer pattern.
        getTrendingFeed(data => {
          this.setState({
            trendingFeedItems: data
          });
        });
        break;
      default:
        throw "Illegal screenName: " + screenName;
    }
  }
  render() {
    let theList;
    const { screenName } = this.props;
    switch (screenName) {
      case Constants.HOT:
        theList = this.state.hotFeedItems;
        break;
      case Constants.TRENDING:
        theList = this.state.trendingFeedItems;
        break;
      default:
        throw "Illegal screenName: " + screenName;
    }
    return (
      <View style={tabStyles.tabContainer}>
        <SfatListView
          onPullToRefresh={this.onPullToRefresh}
          onNoUserId={this.props.onNoUserId}
          navigation={this.props.navigation}
          feedList={theList}
        />
      </View>
    );
  }
}

const tabStyles = StyleSheet.create({
  container: {
    flex: 1,
    height: 60
  },
  tabContainer: {
    flex: 1,
    paddingTop: 8,
    backgroundColor: appColors.backgroundDark
  },
  tabbar: {
    backgroundColor: appColors.backgroundDark,
    flexDirection: "row",
    justifyContent: "center"
  },
  tab: {
    width: Dimensions.get("window").width / 2
  },
  indicator: {
    backgroundColor: appColors.activeColor,
    alignItems: "center",
    justifyContent: "center"
  },
  label: {
    color: "#fff",
    fontWeight: "400"
  }
});
