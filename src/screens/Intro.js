/*jshint esversion: 6 */
import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  TouchableOpacity,
  Image,
  Text,
  View,
  TextInput,
  Button,
  Alert,
  Dimensions,
  InteractionManager
} from "react-native";
import {
  NavigationActions,
  StackActions,
  createStackNavigator
} from "react-navigation";
import Answer from "./Answer.js";
import * as Animatable from "react-native-animatable";
import { dimensions } from "../styles/base.js";
import appColors from "../styles/colors.js";
import * as Constants from "../util/Constants.js";
import { cacheFeedData } from "../managers/DataCachedManager.js";
import LongButton from "../components/buttons/LongButton.js";
import { facebookLogin } from "../managers/FacebookManager.js";
import { pushUserName, getUserByName } from "../managers/FirebaseManager.js";
import * as AppMaster from "../AppMaster.js";
import DefaultPreference from "react-native-default-preference";

const NEXT_SCREEN_START_DELAY = 1000;
const USER_NAME_TAKEN_MESSAGE_DELAY = 1500;

export default class Intro extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: null,
      userName: null,
      userInfoLoaded: false,
      showingFailure: false,
      userNameLabel: "Choose a user name",
      userNameLabelColor: appColors.primaryLight
    };
  }

  componentDidMount() {
    AppMaster.initialize(this.onInitializeDone.bind(this));
  }

  onInitializeDone(userId, userName) {
    cacheFeedData(userId);
    this.setState({
      userId: userId,
      userName: userName,
      userInfoLoaded: true
    });
    if (userId && userName) {
      this.doLoggedInUi();
    }
  }

  doLoggedInUi() {
    setTimeout(() => {
      this.navigateToMain();
    }, NEXT_SCREEN_START_DELAY);
  }

  onFacebookLoginClick() {
    facebookLogin(this.onFacebookLoginDone.bind(this));
  }

  onFacebookLoginDone(result, error) {
    if (error) {
      alert("Error fetching data: " + error.toString());
    } else {
      AppMaster.setUserId(result.id, () => {
        this.setState({
          userId: result.id
        });
      });
    }
  }

  onUserNameChosen() {
    if (!this.state.showingFailure) {
      getUserByName(this.state.userNameLive, (doc, error) => {
        if (doc && doc.data().userName) {
          console.log("User name conflict!");
          this.onUserNameConflict();
        } else {
          console.log("User name is ok");
          AppMaster.setUserName(this.state.userNameLive, () => {
            this.setState({
              userName: this.state.userNameLive
            });
            this.doLoggedInUi();
          });
          pushUserName(
            null,
            this.state.userNameLive,
            this.state.userId,
            error => {
              if (error) {
                alert(error);
              }
            }
          );
        }
      });
    }
  }

  onUserNameConflict() {
    this.setState({
      showingFailure: true,
      userNameLabel: "User name taken",
      userNameLabelColor: appColors.failedColor
    });
    setTimeout(() => {
      this.setState({
        showingFailure: false,
        userNameLabel: "Choose a user name",
        userNameLabelColor: appColors.primaryLight
      });
    }, USER_NAME_TAKEN_MESSAGE_DELAY);
  }

  navigateToMain() {
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: "QANavigator" })]
    });
    this.props.navigation.dispatch(resetAction);
  }

  renderLoginView() {
    return (
      <View style={styles.loginView}>
        <View style={styles.introTextContainer}>
          <Animatable.Text style={styles.welcomeTextPrimary}>
            Sfat
          </Animatable.Text>
          <Animatable.Text style={styles.welcomeTextSecondary}>
            Ask a random{"\n"}person something
          </Animatable.Text>
        </View>
        {this.state.userInfoLoaded && !this.state.userId ? (
          <View
            style={{
              width: "100%",
              alignItems: "center",
              bottom: 40,
              position: "absolute"
            }}
          >
            <LongButton
              buttonStyle={{
                backgroundColor: appColors.facebookButton,
                width: "84%"
              }}
              buttonText="Login with facebook"
              clickListener={this.onFacebookLoginClick.bind(this)}
            />
            <TouchableOpacity
              onPress={this.navigateToMain.bind(this)}
              activeOpacity={0.7}
            >
              <Text
                style={[
                  {
                    color: appColors.primaryLight,
                    textAlign: "center",
                    fontSize: 18,
                    marginTop: 24,
                    marginBottom: 32
                  },
                  this.props.textStyle
                ]}
              >
                skip
              </Text>
            </TouchableOpacity>
          </View>
        ) : null}
      </View>
    );
  }
  renderChooseUserNameViews() {
    return (
      <View style={styles.chooseUserNameView}>
        <Animatable.Text
          style={{
            textAlign: "center",
            marginTop: 80,
            fontFamily: "celevenia",
            fontSize: 90,
            marginTop: 120,
            color: appColors.primaryLight,
            backgroundColor: "transparent"
          }}
        >
          Sfat
        </Animatable.Text>
        <Animatable.Text
          style={
            ([styles.userNameLabel], { color: this.state.userNameLabelColor })
          }
        >
          {this.state.userNameLabel}
        </Animatable.Text>
        <TextInput
          onChangeText={userNameLive => {
            this.setState({ userNameLive });
          }}
          style={styles.userNameInput}
        />
        <LongButton
          buttonStyle={{
            backgroundColor: appColors.purpleButton,
            width: "84%"
          }}
          buttonText="Submit"
          clickListener={this.onUserNameChosen.bind(this)}
        />
      </View>
    );
  }
  renderViews() {
    if (
      !this.state.userInfoLoaded ||
      !this.state.userId ||
      (this.state.userId && this.state.userName)
    ) {
      return this.renderLoginView();
    } else {
      return this.renderChooseUserNameViews();
    }
  }

  render() {
    return (
      <View style={styles.introMainContainer}>
        <Image
          style={styles.bgImage}
          source={require("../images/intro_pattern.png")}
        />
        {this.renderViews()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  introMainContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  loginView: {
    flex: 1,
    position: "absolute",
    height: dimensions.fullHeight,
    width: dimensions.fullWidth,
    alignItems: "center",
    justifyContent: "center"
  },
  userNameInput: {
    height: 45,
    width: "84%",
    marginTop: 16,
    paddingHorizontal: 16,
    backgroundColor: appColors.inputBackground,
    borderRadius: Constants.BUTTON_CORNER,
    shadowColor: "black",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: Constants.SHADOW_OPACITY,
    shadowRadius: Constants.SHADOW_RADIUS
  },
  chooseUserNameView: {
    flex: 1,
    flexDirection: "column",
    position: "absolute",
    height: dimensions.fullHeight,
    width: dimensions.fullWidth,
    alignItems: "center",
    justifyContent: "flex-start"
  },
  introTextContainer: {
    width: "100%",
    marginBottom: 32,
    position: "absolute",
    flexDirection: "column",
    alignItems: "center"
  },
  bgImage: {
    position: "absolute",
    resizeMode: "cover",
    flex: 1,
    height: dimensions.fullHeight,
    width: dimensions.fullWidth
  },
  welcomeTextPrimary: {
    textAlign: "center",
    fontFamily: "celevenia",
    fontSize: 140,
    color: appColors.primaryLight,
    backgroundColor: "transparent"
  },
  welcomeTextSecondary: {
    textAlign: "center",
    fontFamily: "celevenia",
    fontSize: 20,
    color: appColors.primaryLight,
    backgroundColor: "transparent"
  },
  userNameLabel: {
    textAlign: "center",
    fontFamily: "celevenia",
    marginTop: 60,
    fontSize: 20,
    color: appColors.primaryLight,
    backgroundColor: "transparent"
  }
});
