/*jshint esversion: 6 */
import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Image,
  Text,
  View,
  TextInput,
  Button,
  Alert,
  Dimensions,
  InteractionManager,
  TouchableOpacity
} from "react-native";
import styles from "../styles/style.js";
import { createStackNavigator } from "react-navigation";
import * as Animatable from "react-native-animatable";
import * as Constants from "../util/Constants.js";
import LongButton from "../components/buttons/LongButton.js";
import appColors from "../styles/colors.js";
import SfatDialog from "../components/dialogs/SfatDialog.js";
import { getQuestions, pushAnswer } from "../managers/FirebaseManager.js";
import { getUserId, navigateToLogin } from "../AppMaster.js";
const QUESTION_LIMIT = 10;

export default class Answer extends React.Component {
  constructor() {
    super();
    this.state = {
      questions: null,
      dialogVisibility: false,
      showingQuestion: null
    };
  }
  componentDidMount() {
    this.fetchQuestions();
  }

  fetchQuestions() {
    getQuestions(QUESTION_LIMIT, (docs, error) => {
      if (error) {
        alert(error);
      } else {
        this.setState({ questions: docs });
        this.showNextQuestion();
      }
    });
  }

  showNextQuestion() {
    if (this.state.questions.length > 0) {
      this.setState({ showingQuestion: this.state.questions.pop() });
    } else {
      this.setState({ showingQuestion: undefined });
    }
  }
  onChangeQuestionClicked() {
    this.showNextQuestion();
    //out of questions, fetch.
    if (!this.state.showingQuestion) {
      this.fetchQuestions();
    }
  }

  onAnswerSubmitted(error) {
    if (!error) {
      this.answerInput.clear();
      this.showNextQuestion();
    }
    alert(error ? "Error: " + error : "Sent!");
  }

  answerToQuestion() {
    let userId = getUserId();
    if (userId) {
      if (this.state.answer) {
        pushAnswer(
          this.state.answer,
          new Date().toDateString(),
          userId,
          this.state.showingQuestion.id,
          this.onAnswerSubmitted.bind(this)
        );
      } else {
        alert("Please write your answer down :)");
      }
    } else {
      this.setState({
        dialogTitle: "Logged out",
        dialogText: "Continue to login screen?",
        pozText: "Yes",
        negText: "Cancel",
        onPozButtonClick: (() => {
          this.setState({ dialogVisibility: false });
          navigateToLogin(this.props.navigation);
        }).bind(this),
        onNegButtonClick: (() => {
          this.setState({ dialogVisibility: false });
        }).bind(this),
        dialogVisibility: true
      });
    }
  }

  render() {
    const questionTextColor = this.state.showingQuestion
      ? appColors.almostWhite
      : appColors.textColorGreyLight;
    const { navigate } = this.props.navigation;
    return (
      <View style={answerStyle.mainView}>
        <View style={answerStyle.groupView}>
          <Text
            style={[answerStyle.questionText, { color: questionTextColor }]}
          >
            {this.state.showingQuestion
              ? this.state.showingQuestion.data().qText
              : "No more questions"}
          </Text>
          <TextInput
            ref={input => {
              this.answerInput = input;
            }}
            onChangeText={answer => this.setState({ answer })}
            style={answerStyle.baseInput}
          />
          <View
            style={{
              flex: 1,
              alignItems: "center",
              flexDirection: "column",
              justifyContent: "center",
              height: "15%",
              width: "100%"
            }}
          >
            <LongButton
              buttonStyle={{ backgroundColor: appColors.secondaryButton }}
              textStyle={{
                color: appColors.almostWhite,
                textAlign: "center",
                fontSize: 16
              }}
              buttonText="Change Question"
              clickListener={this.onChangeQuestionClicked.bind(this)}
            />
            {this.state.showingQuestion ? (
              <LongButton
                textStyle={{
                  color: appColors.almostWhite,
                  textAlign: "center",
                  fontSize: 16
                }}
                buttonText="Submit answer"
                clickListener={this.answerToQuestion.bind(this)}
              />
            ) : null}
          </View>
        </View>
        <SfatDialog
          onPozButtonClick={this.state.onPozButtonClick}
          onNegButtonClick={this.state.onNegButtonClick}
          alertVisibility={this.state.dialogVisibility}
          dialogTitle={this.state.dialogTitle}
          dialogText={this.state.dialogText}
          pozText={this.state.pozText}
          negText={this.state.negText}
        />
      </View>
    );
  }
}

const answerStyle = StyleSheet.create({
  mainView: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "flex-start",
    backgroundColor: appColors.backgroundDark
  },
  groupView: {
    flex: 0.7,
    flexDirection: "column",
    justifyContent: "flex-start"
  },
  questionText: {
    fontSize: 18,
    textAlign: "center",
    marginTop: 40,
    marginBottom: 32
  },
  baseInput: {
    padding: 8,
    height: 200,
    marginHorizontal: 20,
    backgroundColor: appColors.inputBackground,
    borderRadius: 4
  }
});
