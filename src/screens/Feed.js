/*jshint esversion: 6 */
import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Image,
  Text,
  View,
  TextInput,
  Button,
  Alert,
  Dimensions,
  InteractionManager,
  TouchableOpacity
} from "react-native";
import { createStackNavigator } from "react-navigation";
import * as Animatable from "react-native-animatable";
import appColors from "../styles/colors.js";
import SfatListView from "../components/SfatListView.js";
import { TabView, TabBar, SceneMap } from "react-native-tab-view";
import TabScreen from "./TabScreen";
import SfatDialog from "../components/dialogs/SfatDialog";
import { getAnswerPairs } from "../managers/FirebaseManager";
import * as Constants from "../util/Constants.js";
import { getUserId, navigateToLogin } from "../AppMaster.js";

export default class Feed extends React.Component {
  /* Return object for populate the list */
  constructor(props) {
    super(props);
    this.state = {
      dialogVisibility: false,
      answerPairs: [],
      index: 0,
      routes: [
        { key: "hot", title: "Hot" },
        { key: "trending", title: "Trending" }
      ]
    };
  }

  onNoUserId() {
    this.setState({
      dialogTitle: "Logged out",
      dialogText: "Continue to login screen?",
      pozText: "Yes",
      negText: "Cancel",
      onPozButtonClick: (() => {
        this.setState({ dialogVisibility: false });
        navigateToLogin(this.props.navigation);
      }).bind(this),
      onNegButtonClick: (() => {
        this.setState({ dialogVisibility: false });
      }).bind(this),
      dialogVisibility: true
    });
  }

  render() {
    return (
      <View style={feedStyles.root}>
        <TabView
          navigationState={this.state}
          style={[feedStyles.container, this.props.style]}
          renderTabBar={this.renderTabBar}
          renderScene={SceneMap({
            hot: () => (
              <TabScreen
                onNoUserId={this.onNoUserId.bind(this)}
                navigation={this.props.navigation}
                screenName={Constants.HOT}
              />
            ),
            trending: () => (
              <TabScreen
                onNoUserId={this.onNoUserId.bind(this)}
                navigation={this.props.navigation}
                screenName={Constants.TRENDING}
              />
            )
          })}
          onIndexChange={index => this.setState({ index })}
          initialLayout={{
            width: Dimensions.get("window").width,
            height: Dimensions.get("window").height
          }}
        />
        <SfatDialog
          onPozButtonClick={this.state.onPozButtonClick}
          onNegButtonClick={this.state.onNegButtonClick}
          alertVisibility={this.state.dialogVisibility}
          dialogTitle={this.state.dialogTitle}
          dialogText={this.state.dialogText}
          pozText={this.state.pozText}
          negText={this.state.negText}
        />
      </View>
    );
  }
  renderTabBar = props => (
    <TabBar
      {...props}
      scrollEnabled
      indicatorStyle={feedStyles.indicator}
      style={feedStyles.tabbar}
      labelStyle={feedStyles.label}
      tabStyle={feedStyles.tab}
    />
  );
}

const feedStyles = StyleSheet.create({
  root: {
    flex: 1
  },
  container: {
    flex: 1,
    height: 60
  },
  feedContainer: {
    flex: 1,
    paddingTop: 8,
    backgroundColor: appColors.backgroundDark
  },
  tabbar: {
    backgroundColor: appColors.backgroundDark,
    flexDirection: "row",
    justifyContent: "center"
  },
  tab: {
    width: Dimensions.get("window").width / 2
  },
  indicator: {
    backgroundColor: appColors.activeColor,
    alignItems: "center",
    justifyContent: "center"
  },
  label: {
    color: "#fff",
    fontWeight: "400"
  }
});
