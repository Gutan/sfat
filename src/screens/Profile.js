/*jshint esversion: 6 */
import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Image,
  Text,
  View,
  TextInput,
  Button,
  Alert,
  Dimensions,
  InteractionManager,
  TouchableOpacity
} from "react-native";
import styles from "../styles/style.js";
import { createStackNavigator } from "react-navigation";
import * as Constants from "../util/Constants.js";
import * as Animatable from "react-native-animatable";
import appColors from "../styles/colors.js";
import { getUserName } from "../AppMaster.js";

export default class Profile extends React.Component {
  constructor() {
    super();
  }
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={answerStyle.mainView}>
        <Animatable.Text
          style={{
            textAlign: "center",
            marginTop: 80,
            fontFamily: "celevenia",
            fontSize: 27,
            marginTop: 100,
            color: appColors.almostWhite,
            backgroundColor: "transparent"
          }}
        >
          {getUserName()}
        </Animatable.Text>
        <Image
          style={styles.profileImage}
          source={require("../images/profile_pattern.png")}
        />
      </View>
    );
  }
}

const answerStyle = StyleSheet.create({
  mainView: {
    height: "100%",
    alignItems: "center",
    backgroundColor: appColors.backgroundDark
  },
  profileImage: {
    flex: 1,
    width: 50,
    height: 50,
    resizeMode: "stretch"
  },
  titleText: {
    fontSize: 18,
    color: "#fff",
    textAlign: "center",
    marginTop: 100,
    height: "20%"
  },
  pozButtonStyle: {
    backgroundColor: appColors.primaryButton,
    width: "90%",
    height: 45,
    borderRadius: 4,
    justifyContent: "center",
    alignItems: "center",
    shadowColor: "black",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: Constants.SHADOW_OPACITY,
    shadowRadius: Constants.SHADOW_RADIUS
  },
  negButtonStyle: {
    backgroundColor: appColors.secondaryButton,
    width: "90%",
    height: 45,
    borderRadius: 4,
    marginBottom: 10,
    justifyContent: "center",
    alignItems: "center",
    shadowColor: "black",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: Constants.SHADOW_OPACITY,
    shadowRadius: Constants.SHADOW_RADIUS
  },
  buttonText: {
    color: "#fff",
    textAlign: "center",
    fontSize: 16
  },
  baseInput: {
    height: "40%",
    alignSelf: "stretch",
    position: "relative",
    marginHorizontal: 20,
    backgroundColor: appColors.inputBackground,
    borderRadius: 4
  }
});
