/*jshint esversion: 6 */
import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Image,
  Text,
  View,
  TextInput,
  Button,
  Alert,
  Dimensions,
  InteractionManager,
  TouchableOpacity
} from "react-native";
import styles from "../styles/style.js";
import { createStackNavigator } from "react-navigation";
import * as Constants from "../util/Constants.js";
import * as Animatable from "react-native-animatable";
import LongButton from "../components/buttons/LongButton.js";
import IconButton from "../components/buttons/IconButton.js";
import appColors from "../styles/colors.js";
import {
  setUserId,
  getUserId,
  setUserName,
  navigateToLogin
} from "../AppMaster.js";

export default class Menu extends React.Component {
  constructor(props) {
    super(props);
  }

  onLogOut() {
    setUserName(undefined, null);
    setUserId(undefined, () => {
      navigateToLogin(this.props.navigation);
    });
  }
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={answerStyle.mainView}>
        <Animatable.Text
          style={{
            textAlign: "center",
            marginTop: 80,
            fontFamily: "celevenia",
            fontSize: 90,
            marginTop: 120,
            color: appColors.almostWhite,
            backgroundColor: "transparent"
          }}
        >
          Sfat
        </Animatable.Text>
        {getUserId() ? (
          <LongButton
            buttonStyle={{
              backgroundColor: appColors.redButton,
              position: "absolute",
              bottom: 40,
              width: "84%"
            }}
            buttonText="Log out"
            clickListener={this.onLogOut.bind(this)}
          />
        ) : null}
      </View>
    );
  }
}

const answerStyle = StyleSheet.create({
  mainView: {
    height: "100%",
    alignItems: "center",
    backgroundColor: appColors.backgroundDark
  }
});
